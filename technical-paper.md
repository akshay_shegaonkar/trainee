# [Code Refactoring](https://www.bmc.com/blogs/code-refactoring-explained/)

Often-overlooked aspects of software are code readablility and understandability. The software can work fine with a badly written codebase, but it becomes difficult for anybody to maintain such code.

In such case, we can refactor the codebase by following certain coding patterns and concepts. In the following section, we will be using **Object-Oriented Programming (OOP)** concepts and have an overview of how we can achieve clean and maintainable codebase.

## Object-Oriented Programming

[Object-Oriented Programming or OOP](https://searchapparchitecture.techtarget.com/definition/object-oriented-programming-OOP) is the concept of using ***Classes*** and ***Objects*** to represent data for performing business logic and operations. A *Class* serves as a blueprint for creating *Objects* and usually represents a real-world entity. *Objects* are created using these *Classes* and each *Class* have unique properties and methods. *Class* is a template while *Object* is an instance of the *Class*.

#### Principals of OOP:

- [Class](https://www.geeksforgeeks.org/classes-objects-java/)
- [Object](https://www.geeksforgeeks.org/classes-objects-java/)
- [Encapsulation](https://www.geeksforgeeks.org/encapsulation-in-java/)
- [Abstraction](https://www.geeksforgeeks.org/abstraction-in-java-2/)
- [Polymorphism](https://www.geeksforgeeks.org/polymorphism-in-java/)
- [Inheritance](https://www.geeksforgeeks.org/inheritance-in-java/)
  
Let us look at an example of how we can use this to refactor our code.
_____

Suppose we have a scenario of working with students and teachers data. Below is a valid way of representing them in code.

###### Students

```javascript
student1 = {
    firstName: 'John'.
    lastName: 'Snow',
}

student2 = {
    firstName: 'Thomas'.
    lastName: 'Shelby',
}

student3 = {
    firstName: 'Diana'.
    lastName: 'Prince',
}
```

###### Teachers
```javascript
teacher1 = {
    firstName: 'James'.
    lastName: 'Miller',
    qualification: 'B.Ed',
}

teacher2 = {
    firstName: 'Alexandra'.
    lastName: 'Lambert',
    qualification: 'M.E',
}
```

Here we are creating a new JavaScript Object for each student and teacher. However, as the number of students and teachers grows, it becomes difficult to keep track of every person and manage the code.

Moreover, if we want to print the full names we would have to do:

```javascript
console.log(`${student1.firstName} ${student1.lastName}`);
console.log(`${student2.firstName} ${student2.lastName}`);

console.log(`${teacher1.firstName} ${teacher1.lastName}`);
```

This is cumbersome and we end up repeating a lot of our code.
____

#### 1. Classes

```javascript

// Student Class
class Student {
    constructor(fName, lName, str) {
        this.firstName = fName;
        this.lastName = lName;
        this.stream = str;
    }
}

// Teacher Class
class Teacher {
    constructor(fName, lName, quali) {
        this.firstName = fName;
        this.lastName = lName;
        this.qualification = quali;
    }
}
```

#### 2. Objects

Now we can create the Student and Teacher objects using their Classes.

```javascript
student1 = new Student('John', 'Snow', 'CS')
student2 = new Student('Thomas', 'Shelby', 'Civil')
student3 = new Student('Diana', 'Prince', 'Mechanical')

teacher1 = new Teacher('James', 'Miller', 'B.Ed')
teacher2 = new Teacher('Alexandra', 'Lambert', 'M.E')
```
As you can see, the code has become much more readable.

#### 3. Encapsulation

Encapsulation means hiding information or data. It refers to the ability of objects to provide functionality without providing any execution details. We can make the `firstName` & `lastName` private and expose public functions to get the full name.

```javascript
class Student {
    constructor(fName, lName, str) {
        const firstName = fName; // private
        const lastName = lName; // private
        const stream = str; // private
        this.fullName = () => `${firstName} ${lastName}`; // public
        this.getStream = () => stream; // public
    }
}

class Teacher {
    constructor(fName, lName, quali) {
        const firstName = fName; // private
        const lastName = lName; // private
        const qualification = quali; // private
        this.fullName = () => `${firstName} ${lastName}`; // public
        this.getQualification = () => qualification; // public
    }
}
```
To get the full name, these methods can be called using their object.

```javascript
console.log(student1.fullName()); // John Snow
console.log(student2.fullName()); // Thomas Shelby
console.log(student3.fullName()); // Diana Prince

console.log(teacher1.fullName()); // James Miller
console.log(teacher2.fullName()); // Alexandra Lambert

console.log(student1.firstName()); // undefined
console.log(teacher1.firstName()); // undefined
```

#### 4. Abstraction

Abstraction means implementation hiding. It is a way of hiding the implementation details and only showing the required output. It hides irrelevant details and shows only whats necessary to the outer world.

```javascript
class Student {
    ...
    
    bio = () => `My name is ${this.getName()}. I am a ${this.getStream()} student.`;
}

class Teacher {
    ...

    bio = () => `My name is ${this.getName()}. I am a ${this.getQualification()} graduate.`
}
```

#### 5. Polymorphism

The ability to call the same method on different objects and have each of them respond in their own way is called polymorphism. We have effectively achieved polymorphism in the above code as calling the same `bio()` and `fullName()` method results in a different output for each object.

Until this point, we have refactored our code quite a bit and the code is now understandable at a glance. But we can go on a step further.

#### 6. Inheritance

We observe that both the Student and Teacher classes have common properties i.e. `firstName`, `lastName`. We can extract these properties into another ***Class Person*** and have the Student and Teacher class inherit those properties from the Person class. So the Person class will act as parent class and Student and Teacher classes will be the child classes.

```javascript
class Person {
    constructor(fName, lName) {
        const firstName = fName;
        const lastName = lName;
        this.fullName = () => `${firstName} ${lastName}`;
    }
}

class Student extends Person {
    constructor(fName, lName, str) {
        super(fName, lName);
        const stream = str;
        this.getStream = () => stream;
    }

    ...
}

class Teacher extends Person {
    constructor(fName, lName, quali) {
        super(fName, lName, gender);
        const qualification = quali;
        this.getQualification = () => qualification;
    }

    ...
}
```

Now we can create the objects using the same code as we did earlier and also get the full name using the same. And if we want to make changes to any function we have to do it in just one place. We can define class-specific functions that will be available to objects of those classes only.

____

Above OOP concepts can be applied to any codebase to make it more readable, scalable, changeable, and manageable.